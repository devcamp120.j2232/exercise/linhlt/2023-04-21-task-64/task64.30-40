package com.devcamp.task5820jpadrinklist.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5820jpadrinklist.model.CDrink;

public interface DrinkRepository extends JpaRepository<CDrink, Long>{
    CDrink findById(long id);
}

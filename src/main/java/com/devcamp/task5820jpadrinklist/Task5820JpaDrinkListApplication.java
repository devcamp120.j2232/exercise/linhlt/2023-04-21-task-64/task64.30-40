package com.devcamp.task5820jpadrinklist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5820JpaDrinkListApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5820JpaDrinkListApplication.class, args);
	}

}
